import { createContext, useState, useEffect } from 'react'
import { getUser } from '../api'
import { CodeError } from '../utils'

const AuthContext = createContext({
    user: null,
    login: () => console.log('login'),
    avatar: null,
    key: null,
    authReady: false
})

export const AuthContextProvider = ({children}) => {
    const [ user, setUser ] = useState(null);
    const [ authReady, setAuthReady ] = useState(false);

    useEffect(() => {

        const fetchData = async () => {
            const key = JSON.parse( localStorage.getItem("teamwork-api-key") )
            if( !key ) throw new CodeError( 'key has not been added', 401 )
            const data = await getUser(key)
            setUser( data )
            setAuthReady(true)
        }
        fetchData()

        .catch( (err) => {
            setAuthReady(true)
            console.error(err)
        });
    }, [])

    return (
        <AuthContext.Provider value={{user, authReady}}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContext
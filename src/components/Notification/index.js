import React from "react";
import { motion, AnimatePresence } from "framer-motion";
import { CloseButton } from "../Buttons/close-button";

export const Notifications = ({ notifications, removeNotification }) => {

  return (
    <div className="fixed top-20 right-5 z-20">
      <ul className="flex flex-col gap-5">
        <AnimatePresence initial={false}>
          {notifications.map( ( notification ) => (
            <motion.li
              className={`dark:bg-slate-700 bg-gray-100 rounded-md pt-10 p-4 px-6 relative border-b-4 ${notification?.state === 'error' ? 'border-red-500' : 'border-green-500'} `}
              key={notification.id}
              initial={{ opacity: 0, y: 50, scale: 0.3 }}
              animate={{ opacity: 1, y: 0, scale: 1 }}
              exit={{ opacity: 0, scale: 0.5, transition: { duration: 0.2 } }}
            >
              <CloseButton
                close={ () => removeNotification( notification.id ) }
                dim={30}
              />
              <p>{notification.content}</p>

            </motion.li>
          ))}
        </AnimatePresence>
      </ul>
    </div>
  );
};

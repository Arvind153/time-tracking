import { motion } from 'framer-motion'
import React from 'react'

export const AxisLeft = ({yScale, innerWidth, tickOffset=3}) => {

    return yScale.ticks(10).map( (tickValue, i) => {

        const key = tickValue
        const y = yScale(tickValue)

        return (
        <g key={i}>
            <motion.line 
                x2={innerWidth}
                className="dark:stroke-stone-600 stroke-stone-300"
                strokeWidth="3"
                initial={{
                    y: 0,
                    opacity: 0
                }}
                animate={{ 
                    y: y,
                    opacity: 1
                }}
                transition={{ type: 'anticipate' }}
            /> 
            <motion.text  
                dy="0.32em" 
                x={-tickOffset} 
                style={{textAnchor: 'end'}}
                className="dark:fill-white"
                initial={{
                    y: 0,
                    opacity: 0
                }}
                animate={{ 
                    y: y,
                    opacity: 1
                }}
                transition={{ type: 'anticipate' }}
            >
                {Math.abs(key)}
            </motion.text>
 
        </g>
    )
})
}
import React, { useState } from "react";
import {
	stack,
	area,
	curveBumpX,
	stackOffsetSilhouette,
} from "d3-shape";
import { schemeDark2 } from "d3-scale-chromatic";
import { scaleLinear, scaleOrdinal } from "d3-scale";
import { min } from "d3-array";
import { timeFormat, timeDay, scaleTime } from "d3"

import { Tooltip } from "../Tooltip"
import { XLabel } from '../Labels/XLabel'
import { YLabel } from '../Labels/YLabel'

import { AxisLeft } from './AxisLeft'

import { motion } from "framer-motion";
// import { Weekend } from "../BarChart/Weekend"

export const StreamChart = ({ data, width, height, keys, margin }) => {
    
	const innerHeight = height - margin.top - margin.bottom
	const innerWidth = width - margin.left - margin.right

	const xAxisLabelOffset = 75
    const yAxisLabelOffset = 40
    // const xTickOffset = 20
    const yTickOffset = 10
	
	// Color for each category
	const colorScale = scaleOrdinal().domain(keys).range(schemeDark2);

	// Accessor function to get the year and then build scale from it
	const xValue = (d) => d.date;
	// const xScale = scaleBand().domain(extent(data, xValue)).range([0, innerWidth]);
	const end_date = new Date()
    end_date.setHours(23,59,59,999)

	const xScale = scaleTime()
	.domain( [ min(data, xValue), end_date ] )
	.rangeRound( [0, innerWidth] )

	const yScale = scaleLinear().domain([-4.5, 4.5]).range([innerHeight, 0]);

	// could do some filtering here
	const stackData = data;
	// console.log({data})
	// Setup the layout of the graph
	const stackLayout = stack()
		.offset(stackOffsetSilhouette)
		.keys(keys);

	// Using the stackLayout we get two additional components for y0 and y1.
	// For x we want to get the date from the original data, so we have to access d.data
	const stackArea = area()
		.x((d) => xScale(d.data.date))
		.y0((d) => yScale(d[0]))
		.y1((d) => yScale(d[1]))
		.curve(curveBumpX);


	const [opacity, setOpacity] = useState(0);
	const [text, setText] = useState("initialState");

	//Interactivity function #1: Hovering
	const handleMouseover = (d) => {
		setOpacity(1);
	};
	//Interactivity function #2: Moving
	const handleMousemove = (d) => {
		setText(d.key);
	};
	//Interactivity function #3: Leaving
	const handleMouseleave = (d) => {
		setOpacity(0);
		setText("initialState");
	};

	
	// Generate path elements using React 
	// Mouseovers and opacity are optional for interactivity
	const stacks = stackLayout(stackData).map((d, i) => {
		// console.log({d})
		// console.log({text})
		return(
		<motion.path
			key={d.key}
			// d={stackArea(d)}

			fill={colorScale(d.key)}
			// style={{
			// 	// opacity: text === "initialState" || d.key === text ? 1 : 0.2,
			// }}
			onMouseEnter={() => {
				handleMouseover(d);
			} }
			onMouseMove={() => {
				handleMousemove(d);
			} }
			onMouseLeave={() => {
				handleMouseleave(d);
			} }
			initial={{
				// opacity: text === "initialState" || d.key === text ? 1 : 0.2,
				pathLength: 0,
				scaleY: 0,
				y: innerHeight / 2,
			}}
			animate={{
				opacity: text === "initialState" || d.key === text ? 1 : 0.2,
				pathLength: 1,
				scaleY: 1,
				d: stackArea(d),
				y: 0,
			}}
			transition={{
				delay: i * 0.01,
				duration: .5,
				ease: "easeInOut",
			}} />
	)});

  return (
    <svg className="mt-10" width={width} height={height}>
        <Tooltip opacity={opacity} text={text} y={30} />
        <g transform={`translate(${margin.left}, ${margin.top})`}>
			<AxisLeft yScale={yScale} innerWidth={innerWidth} tickOffset={yTickOffset} />
            <g transform={`translate(23, 0)`}>
				{stacks}
			</g>
        
            { xScale.ticks(data.length > 25 ? 25 : data.length).map(  ( tickValue, i ) => {
                  const ticksArray = xScale.ticks(timeDay)

                  const tickWidth = xScale(ticksArray[1]) - xScale(ticksArray[0])
                return (
                <g className="axis axis-x" key={i} transform={`translate(${xScale(tickValue) }, 0)`}>
                    <text 
                        x={ tickWidth /2 }
                        y={innerHeight + 10}
                        dy=".71em"
                        className="dark:fill-white"
                        style={{textAnchor: 'middle'}}>
                        {timeFormat("%a")(tickValue)[0]}
                    </text>
                    <text 
                        x={ tickWidth /2  }
                        y={innerHeight + 30}
                        dy=".71em"
                        className="dark:fill-white"
                        style={{textAnchor: 'middle'}}>
                        {timeFormat("%d")(tickValue)}
                    </text>
                    
                </g>
                )} ) 
            }
			
            <YLabel label="Hours" height={innerHeight} offset={yAxisLabelOffset} />
            <XLabel label="Date" height={innerHeight} width={innerWidth} offset={xAxisLabelOffset} />
        </g>
    </svg>
  );
};
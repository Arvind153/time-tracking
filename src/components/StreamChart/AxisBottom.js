import { timeDay } from 'd3'
import React from 'react'

export const AxisBottom = ({xScale, innerHeight, tickFormat, xAxisTickFormatDate, tickOffset=3, ticks=10}) => {
    const ticksArray = xScale.ticks(timeDay)
    const tickWidth = xScale(ticksArray[1]) - xScale(ticksArray[0])
    // console.log({ticksArray})
    return xScale.ticks(ticks > 25 ? 25 : ticks).map( ( tickValue, i ) => {
        // if( i == 0 ) return
        return (
        <g className="axis axis-x" key={i} transform={`translate(${xScale(tickValue)}, 0)`}>
            { false ?
            <rect 
                fill="currentColor" 
                y={ 0 }
                width={ tickWidth -7 }
                height={ innerHeight }
                className="text-slate-100" >
            </rect>
            : null }

            <text 
                x={ tickWidth / 2 }
                y={innerHeight + tickOffset}
                dy=".71em"
                className="dark:fill-white"
                style={{textAnchor: 'middle'}}>
                {xAxisTickFormatDate(tickValue)[0]}
            </text>
            <text 
                x={ tickWidth / 2  }
                y={innerHeight + tickOffset + 20}
                dy=".71em"
                className="dark:fill-white"
                style={{textAnchor: 'middle'}}>
                {tickFormat(tickValue)}
            </text>
        </g>   
        )
    })

}
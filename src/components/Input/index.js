import { LoginIcon } from '@heroicons/react/outline'
import React, { useState, useEffect } from 'react';

import { getUser, validateKey } from '../../api'


export default function Input() {
    const [key, setKey] = useState('');
    
    useEffect(() => {
      const key = JSON.parse( localStorage.getItem("teamwork-api-key") )
      if( key ) setKey( key )
    }, [key])

    const validate = () => {
        if( !validateKey( key ) ) return
        localStorage.setItem("teamwork-api-key", JSON.stringify( key ));
        getUser().then( res => {
          if( res?.id ) window.location.href = '/'
        } )
    }

    return (
      <div className="max-w-5xl pt-10 mx-auto">
        <p>Your API key will be used to connect to Teamwork, to fetch the time you've tracked; we don't save your API key in a database, the only place it's stored is in your browsers local storage.</p>
        <p className=" mb-5">You can find your API key <a className="text-orange-500 dark:text-purple-500" rel="noreferrer" target="_blank" href="https://support.teamwork.com/projects/using-teamwork/locating-your-api-key#:~:text=To%20locate%20your%20API%20key,then%20copy%20your%20API%20key.">here.</a></p>
        <label htmlFor="api-key" className="block text-lg font-medium">
          Teamwork API key
        </label>
        <div className="relative flex h-16 mt-1 mb-5 rounded-md shadow-sm">
          <input
            type="text"
            name="api-key"
            id="api-key"
            className="block w-full pr-12 border-gray-300 rounded-tl-lg rounded-bl-lg pl-7 sm:text-sm focus:bg-gray-50 outline-0 dark:bg-slate-700"
            placeholder="****************"
            value={ key }
            onChange={ e => setKey( e.target.value ) }
          />
          <button onClick={ validate } className="flex items-center p-3 text-white rounded-tr-lg rounded-br-lg bg-violet-600 hover:bg-violet-700">
            <LoginIcon className="w-12 h-12" />
          </button>
        </div>
      </div>
    )
}
  
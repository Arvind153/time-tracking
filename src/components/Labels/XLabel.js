export const XLabel = ({ offset, height, width, label}) => (
    <text 
        className="label-x text-xl dark:fill-white" 
        x={width/2} 
        y={height + offset} 
        textAnchor="middle">
        {label}
    </text>
)
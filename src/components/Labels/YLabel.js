export const YLabel = ({ offset, height, label}) => (
    <text 
        className="label-y text-xl dark:fill-white" 
        textAnchor="middle"
        transform={`translate(${-offset}, ${height/2}) rotate(-90)`}>
            {label}
    </text>
)
import { AnimatePresence, motion } from 'framer-motion';
import React, { useState, useEffect } from 'react'

export default function PopularTasks( { tasks, handleHover } ) {
    const [data, setData] = useState([]);
    useEffect( () => {
        let sortable = [];
        let counts = {}
        tasks.forEach( element => {
            
            counts[ element.taskId ] = { 
                'count': ( counts[ element?.taskId ]?.count || 0 ) + 1, 
                'hours': ( counts[ element.taskId ]?.hours || 0) + element.hours,
                'task': element.taskName,
                'project': element.project
            }
        })
        for (const count in counts) {
            sortable.push([count, counts[count]]);
        }

        sortable.sort(function(a, b) {
            return b[1]?.hours - a[1]?.hours;
        });
        console.log({sortable})
        sortable.forEach( (task, i) => sortable[i][1].hours = task[1].hours.toFixed(2) )
        setData( sortable.slice(0, 3) )
    }, [tasks])

    return (
      <>
        <h3 className="mb-5 text-lg">Popular items</h3>
        <div className="flex flex-col gap-[1.15rem] justify-between">
            <AnimatePresence>
            { 
                data.map( ( task, i ) => {
                    return (
                <motion.a 
                    href={`https://seoworksshef.teamwork.com/#/tasks/${task[0]}`} 
                    target="_blank"
                    className="p-3 px-5 rounded-lg dark:bg-gray-800 bg-stone-100 hover:dark:bg-gray-900 transition-all" 
                    onMouseEnter={() => handleHover(task[0])}
                    onMouseLeave={() => handleHover(false)}
                    key={task[0]}
                    animate={{ opacity: 1, delay: i*0.3 }}
                    transition={{ delay: i*0.3 }}
                    exit={{ opacity: 0 }}
                    initial={{ opacity: 0 }}
                >
                    {task[1]?.task} { task[1]?.project ? `|| ${task[1]?.project}` : '' } <span className="text-gray-500 dark:text-gray-400">{task[1]?.hours} hours</span>
                </motion.a> )} )
            }
            </AnimatePresence>
        </div>
      </>
    )
}
import * as React from "react";
import { motion } from "framer-motion";

const Path = props => (
  <motion.path
    stroke="currentColor"
    strokeWidth="3"
    // stroke="hsl(0, 0%, 18%)"
    strokeLinecap="round"
    className="dark:text-gray-200"
    {...props}
  />
);

export const CloseButton = ({ close, dim }) => (
  <motion.button onClick={close} className="close absolute right-2 top-2" whileHover={{scale:1.1, rotate: [0, 10, -10, 0], transiton: 'linear'}}>
    <svg className="dark:text-gray-200" width={dim} height={dim} viewBox="0 0 23 23" fill="currentColor">
      <Path d="M 3 16.5 L 17 2.5" />
      <Path d="M 3 2.5 L 17 16.346" />
    </svg>
  </motion.button>
);

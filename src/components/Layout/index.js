
import Footer from '../Footer'
import Header from '../Header'

export default function Layout({ children }) {
  return (
    <>
        <Header />
        <main className="px-5 transition-all bg-gradient-to-br from-white to-zinc-50 dark:from-slate-800 dark:to-stone-900 dark:text-white text-gray-700" style={{minHeight: "calc(100vh - 128px)"}}>{children}</main>
        <Footer />
    </>
  )
}
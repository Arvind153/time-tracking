import { motion } from 'framer-motion'
import React, { useState } from 'react'
import Weekend from './Weekend'

export const Marks = ({
    data, 
    xScale, 
    yScale, 
    innerHeight,
    chartType,
    yScaleLinear,
    hoverId,
    text,
    handleMouseover,
    handleMousemove,
    handleMouseleave
}) => {
    const [ activeId, setActiveId ] = useState(false)
    const handleHover = (id) => setActiveId( id )



    return (     
    <g className="marks">
        {data.map( (d, i) => {
            // if( i === 0 ) return [];
            let height = 0;
            let hours = 0
            const width = ( xScale( d.x1 ) - xScale( d.x0 ) ) - 5
            // console.log({d})
            return (
                d.data.map( (item, j) => {
                const startDate = new Date()
                const endDate = new Date()
                
                const dataDate = new Date(item.date)
                
                const dateMinutes = dataDate.getMinutes()
                const dateHours = dataDate.getHours()
             
                hours = hours + item.hours
            
                startDate.setHours( dateHours, dateMinutes, 0 )

                const additionalMinutes = ( item.hours % 1 * 60 )
                endDate.setHours( dateHours + item.hours, dateMinutes + additionalMinutes, 0 )
                
                const y = chartType === 'stack' ? yScaleLinear( hours ) : yScale( endDate )
                if( chartType === 'stack' ) {
                    height = innerHeight - yScaleLinear(  item.hours )
                } else {
                    height = yScale( startDate ) - yScale( endDate )
                }

                const formattedDate = new Date( d.x0 ).toLocaleDateString('en-GB')
                // console.log({item})
                return (
                    <>
                    { d.weekend ? 
                    <Weekend y={y} width={width} item={item} x={xScale( d.x0 )} height={innerHeight} i={i} /> :
                    <a href={`https://seoworksshef.teamwork.com/#/tasks/${item?.taskId}`} key={item?.taskId}>
                        <motion.rect
                            transition={{ duration: '.5', type: 'spring', delay: (i*0.01) }}
                            initial={{
                                y: y,
                                opacity: 0,
                                height: 0,
                                // opacity: text === "initialState" || d.key === text ? 1 : 0.2,
                                pathLength: 0
                            }}
                            animate={{ 
                                y: y,
                                opacity: 1,
                                height,
                                // opacity: text === "initialState" || d.key === text ? 1 : 0.2,
                                pathLength: 1,
                            }}

                            whileTap={{ scale: 0.9 }}
                            key={ i+j }
                            x={ xScale( d.x0 ) }
                            width={ width }
                            fill={ "currentColor" }
                            strokeWidth="2" strokeLinecap="round"
                            data-hours={ item.hours }
                            className={ ( +item?.taskId === +hoverId || +item?.taskId === +activeId ? `text-teal-400` : `dark:text-purple-700 text-orange-500` ) + ` relative duration-500 origin-center transition-colors`}
                            
                            onMouseEnter={() => {
                                handleHover(item?.taskId)
                                handleMouseover(item);
                            }}
                            onMouseMove={() => {
                                handleMousemove(item);
                            }}
                            onMouseLeave={() => {
                                handleMouseleave(item);
                                handleHover(false)
                            }}
                        >
                            <title>{ `${formattedDate} - ${item.hours.toFixed(1)} hours - ${item.taskName}` }</title>
                        </motion.rect>
                    </a>
                    }
                    </>
                )
                }) 
            )
        })}
    </g>
)}
import React from 'react'
export default function Weekend({ y, x, width, height, item, i }) {

    return (
        <rect
            key={ item.date }
            x={ x }
            width={ width }
            height={ height }
            fill={ "currentColor" }
            strokeWidth="2" strokeLinecap="round"
            data-hours={item.hours}
            className="text-slate-200 dark:text-gray-800"
        />  
    )
}
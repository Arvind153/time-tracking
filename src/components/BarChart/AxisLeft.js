import { motion } from 'framer-motion'
import React from 'react'

export const AxisLeft = ({yScale, innerWidth, yScaleLinear, chartType, tickOffset=3}) => {
    const ticksObj = chartType === 'stack' ? yScaleLinear : yScale
    const tickNo = chartType === 'stack' ? 10 : 12

    return ticksObj.ticks(tickNo).map( (tickValue, i) => {

        const key = chartType === 'stack' ? tickValue : tickValue.getHours()
        const y = chartType === 'stack' ? yScaleLinear(tickValue) : yScale(tickValue)

        return (
        <g key={i}>
            <motion.line 
                x2={innerWidth}
                className="dark:stroke-stone-600 stroke-stone-300"
                strokeWidth="3"
                initial={{
                    y: 0,
                    opacity: 0
                }}
                animate={{ 
                    y: y,
                    opacity: 1
                }}
                transition={{ type: 'anticipate' }}
            /> 
            <motion.text  
                dy="0.32em" 
                x={-tickOffset} 
                style={{textAnchor: 'end'}}
                className="dark:fill-white"
                initial={{
                    y: 0,
                    opacity: 0
                }}
                animate={{ 
                    y: y,
                    opacity: 1
                }}
                transition={{ type: 'anticipate' }}
            >
                {key}
            </motion.text>
 
        </g>
    )
})
}
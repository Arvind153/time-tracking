import React, { useState } from 'react'
import { 
    scaleLinear, 
    extent,
    min,
    scaleTime, 
    timeFormat,
    scaleOrdinal, 
    bin,
    timeDays,
    sum
} from 'd3'

import { Tooltip } from '../Tooltip'
import { AxisLeft } from './AxisLeft'
import { AxisBottom } from './AxisBottom'
import { Marks } from './Marks'
import { YLabel } from '../Labels/YLabel'
import { XLabel } from '../Labels/XLabel'

function addDays( date, days ) {
    const dateValue = new Date( date );
    dateValue.setDate(date.getDate() + days);
    return dateValue;
}

const xValue = d => d.date
const xAxisLabel = 'Date'
const yValue = d => d.hours

const start_of_day = new Date()
start_of_day.setHours(7,0,0,0)
const end_of_day = new Date()
end_of_day.setHours(19,0,0,0)

export default function BarChart({ data, width, height, chartType, date, hoverId, margin }) {
    
    const yAxisLabel = chartType === 'stack' ? 'Hours' : 'Time'
    const xAxisLabelOffset = 75
    const yAxisLabelOffset = 40
    const xTickOffset = 10
    const yTickOffset = 10
    
    const innerHeight = height - margin.top - margin.bottom
    const innerWidth = width - margin.left - margin.right
    // const [opacity, setOpacity] = useState(0);
    // const [text, setText] = useState("initialState");

    const [opacity, setOpacity] = useState(0);
	const [text, setText] = useState("initialState");

	//Interactivity function #1: Hovering
	const handleMouseover = (d) => {
		setOpacity(1);
	};
	//Interactivity function #2: Moving
	const handleMousemove = (d) => {
		setText(`${d.taskName} || ${d.project} - ${d.hours.toFixed(1)} hours`);
	};
	//Interactivity function #3: Leaving
	const handleMouseleave = (d) => {
		setOpacity(0);
		setText("initialState");
	};

    const end_date = new Date();
    end_date.setHours(23,59,59,999);

    let start_date = min( data, xValue )
    start_date.setHours(0,0,0,0)

    const xScale = scaleTime()
        .domain( [ start_date, end_date] )
        .rangeRound( [0, innerWidth] )
  
    const [start] = xScale.domain()
    start.setMinutes(0)

    const binnedData = bin()
        .value( xValue )
        .domain( xScale.domain() )
        // .rangeBands([0, 1], 0.5)
        .thresholds( timeDays(start_date, end_date) )
        (data)
        .map( ( array, i, row ) => {
            const dayOfWeek = array.x0.getDay();
            const isWeekend = (dayOfWeek === 6) || (dayOfWeek  === 0); // 6 = Saturday, 0 = Sunday
            
            const x0 = array.x0;
            let x1 = array.x1;

            // array.x0.setHours(0,0,0,0);
            // array.x1.setHours(0,0,0,0);
            
            if (i + 1 === row.length) {
                x1 = addDays( array.x1, 1 )
            }

            const payload = {
                x0,
                x1,
            }
            
            if( isWeekend ) {
                payload.data =  [{date: start, hours: 10, id: new Date().getTime()}];
                payload.weekend = true
            } else {
                payload.data = array
                payload.y = sum(array, yValue)
                payload.data.id = new Date().getTime()
            }    

            return payload

        })

    const yScale = scaleTime()
        .domain( [ start_of_day, end_of_day ] )
        .range([  innerHeight, 0 ])

    const yScaleLinear = scaleLinear()
        .domain( extent(binnedData, d => d.y) )
        .range([ innerHeight, 0 ])
        .nice()
    
    const colorScale = scaleOrdinal()
        .domain( binnedData.map( d => d.y) )
        .range(["#de6262","#e5756b","#e97e70","#ed8a76","#f39a7e","#f9aa85","#ffb88c"])
        
    const xAxisTickFormat = timeFormat("%d")
    const xAxisTickFormatDate = timeFormat("%a")

    return (   
        <svg height={height} width={width} className="mt-10" >
           
            <Tooltip text={text} opacity={opacity} y={20} />
           <g transform={`translate(${margin.left}, ${margin.top})`} >
                <AxisBottom 
                    xScale={xScale} 
                    innerHeight={innerHeight} 
                    tickFormat={xAxisTickFormat} 
                    xAxisTickFormatDate={xAxisTickFormatDate} 
                    tickOffset={xTickOffset} 
                    ticks={binnedData.length} />
                <AxisLeft 
                    chartType={chartType}
                    yScaleLinear={yScaleLinear} 
                    yScale={yScale} 
                    innerWidth={innerWidth} 
                    tickOffset={yTickOffset} 
                />
                <Marks 
                    chartType={chartType} 
                    data={binnedData} 
                    ticks={binnedData.length} 
                    colorScale={colorScale}
                    innerHeight={innerHeight} 
                    tickOffset={yTickOffset} 
                    xScale={xScale} 
                    yScale={yScale}
                    yScaleLinear={yScaleLinear} 
                    yValue={yValue} 
                    xValue={xValue}
                    hoverId={hoverId}
                    handleMouseover={handleMouseover}
                    handleMousemove={handleMousemove}
                    handleMouseleave={handleMouseleave}
                    text={text}
                />
                <YLabel label={yAxisLabel} offset={yAxisLabelOffset} height={innerHeight} />
                <XLabel label={xAxisLabel} offset={xAxisLabelOffset} height={innerHeight} width={innerWidth} />
            </g>
        </svg>
    )
}
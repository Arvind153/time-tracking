import React, { useState, useEffect, useContext, useRef, useCallback } from "react"
import { motion, AnimatePresence } from "framer-motion";
import { getTime, getTask, getProject } from '../../api'
import Layout from '../Layout'
import InfoBox from '../InfoBox'
import BarChart from '../BarChart'
import { StreamChart } from '../StreamChart'
import AuthContext from '../../context/authContext';
import { formatDate } from '../../utils'
import DateSelector from '../DateSelector'
import PopularTasks from '../PopularTasks'
import RepeatingTime from '../RepeatingTime'
import Modal from '../Modal'
import { Notifications } from '../Notification'
import { Loader } from '../Loading'
import { timeDay, timeDays } from 'd3'

// function useHookWithRefCallback() {
//     const ref = useRef(null)
//     const [width, setWidth] = useState( window.innerWidth *0.85 )
//     const setRef = useCallback( node => {
//       if (ref.current) {
//         // Make sure to cleanup any events/references added to the last instance
//       }
      
//       if (node) {
//         // Check if a node is actually passed. Otherwise node would be null.
//         // You can now do what you need to, addEventListeners, measure, etc.
//         setWidth( node.offsetWidth )
//       }
      
//       // Save a reference to the node
//       ref.current = node
//     }, [])
    
//     return [setRef]
// } 

export default function Dashboard() {
    const { user, authReady } = useContext(AuthContext)
    const [ timeLogs, setTimeLogs ] = useState([])
    const [ projectNames, setProjectNames ] = useState([])
    const [ loading, setLoading ] = useState(true)
    const [ hoverId, setHoverId ] = useState(false)
    const [ groupedData, setGroupedData ] = useState([])
    // const [ holidays, setHolidays ] = useState([])
    
    const [ dateValue, setDateValue ] = useState( formatDate( new Date(Date.now() - 30 * 24 * 60 * 60 * 1000) ) )
    const [width, setWidth] = useState( 0 )
    
    const [chartType, setChartType] = useState('stack')
    const [modalOpen, setModalOpen] = useState(false);
    const [rerender, setRenderer] = useState(false);
    const [repeatingTasks, setRepeatingTasks] = useState( JSON.parse( localStorage.getItem("tw-repeating-tasks") ) );
    const [notifications, setNotifications] = useState([]);
    
    // const [ref] = useHookWithRefCallback()
    const svgRef = useRef(null)

    const ref = useCallback(node => {
        if (node !== null) {
            //setHeight(node.getBoundingClientRect().height);
            setWidth(node.getBoundingClientRect().width);
        }
    }, []);
    
    const close = () => setModalOpen(false);
    const open = () => setModalOpen(true);

    const handleDateChange = date => {
        setLoading( true )
        setChartType('stack')
        setDateValue(date)
    }
    const handleHover = id => setHoverId(id)
    const handleRerender = () => setRenderer( !rerender )
    const addTask = e => setNotifications([...notifications, {id: e.id, content: `You have logged ${e.duration} minutes at ${e.startTime} on ${e.date}`}])
    const addErrorNotification = e => setNotifications([...notifications, {id: e.id, content: `${e?.error?.title?.toUpperCase()} - ${e?.error?.detail} `, state: 'error'}])
    const addLocalTask = ( payload ) =>  setRepeatingTasks( [ ...repeatingTasks, payload] ) 
    const removeNotifications = ( id ) => setNotifications( notifications.filter( notification => notification?.id !== id ) )
    

    const removeLocalTask = ( task ) => {
        const updatedLocalTasks = repeatingTasks.filter( repeatingTask => {
            return !( repeatingTask.taskId === task?.taskId && repeatingTask?.startTime === task?.startTime && repeatingTask?.duration === task?.duration ) 
        })
        setRepeatingTasks( updatedLocalTasks )
    }

    const height = 450
    const margin = {
        top: 50,
        left: 60,
        bottom: 90,
        right: 0
    }

    let streamChart
    if( chartType === 'stream' && groupedData.length ) {
        streamChart = <StreamChart innerRadius={50} outerRadius={300} data={groupedData} width={width} height={height} keys={projectNames} margin={margin} /> 
    } else if( chartType === 'stream' ) {
        streamChart = <Loader />
    }
    
    useEffect( () => {
        if( !user ) {
            if( authReady ) {
                window.location.replace("/settings")
            }
            return
        }
        function handleResize() {
            ref(svgRef?.current)
        }
        
        window.addEventListener( 'resize', handleResize )
        
        async function getData() { 

            const timeData = await getTime(user?.id, dateValue)
            setTimeLogs( timeData ) 
  
            let uniqueTaskIds = [...new Set(timeData.map(item => item.taskId)) ]
            let uniqueProjectIds = [ ...new Set(timeData.map(item => item.projectId)) ]
            
            // Get task details like the task name
            let taskDetails = []
            for( let i=0; i<uniqueTaskIds.length; i++) {
                const taskId = uniqueTaskIds[i]
                if( taskId === undefined ) continue
                const taskObj = await getTask( taskId )
                if( taskObj?.task?.name ) {
                    taskDetails = [ ...taskDetails, { name: taskObj?.task?.name, id: taskId, completed: taskObj.task?.completedOn } ]
                }
            }
            
            // Get the project details like the project name
            let projectDetails = []
            for( let i=0; i<uniqueProjectIds.length; i++) {
                const projectId = uniqueProjectIds[i]
                if( projectId === undefined ) continue
                const taskObj = await getProject( projectId )
                if( taskObj?.project?.name ) {
                    projectDetails = [ ...projectDetails, { name: taskObj?.project?.name, id: projectId, hours:0 } ]
                }
            }

            // Add up all the time tracked on the project
            timeData.forEach( row => {
                projectDetails.forEach( pro => {
                    
                    if( row.projectId === pro.id) {
                        pro.hours += +row.hours
                    }
                } )
            } )

            // Round the project hours to two decimal places
            projectDetails.forEach( (pro, i) => projectDetails[i].hours = pro.hours.toFixed(2))

            // Update the data object with the task name and the project details
            const updatedData = timeData.map( row => {
                const task = taskDetails.filter( t => t.id === row.taskId )
                const project = projectDetails.filter( t => t.id === row.projectId )
                if( task ) {
                    row.taskName = task[0]?.name
                    row.completed = task[0]?.completed ? true : false
                }
                if( project ) {
                    row.projectName = `${project[0]?.name} - ${project[0]?.hours} hours`
                    row.project = project[0]?.name
                    row.projects = projectDetails
                }
                return row
            }) 
            
            // Update logs
            setTimeLogs( updatedData )

            const start = updatedData[0]?.date
            const end = new Date()
            
            const projNames = projectDetails.map( pro => `${ pro.name } - ${ pro.hours } hours` )

            setProjectNames( projNames )
            const newObj = {}
            projNames.forEach( pro => newObj[pro] = 0 )
            const dateRange = timeDays(start, end).map( date => { 
                return {...newObj, date} 
            })
            .map( d => {
                const dayOfWeek = d.date.getDay();
                const isWeekend = (dayOfWeek === 6) || (dayOfWeek  === 0); // 6 = Saturday, 0 = Sunday
                if( isWeekend ) d.isWeekend = true 
                return d
            })

            const groupedData = updatedData
               
                .reduce( (acc, cur) => {
                    acc.forEach( (item, i) => {
                        if( item?.date.getTime() === timeDay( cur.date ).getTime() ) {
                            acc[i][cur.projectName] = acc[i][cur.projectName] + cur.hours
                        } 
                    } )
                    return acc
            }, dateRange )
            setGroupedData( groupedData )
            setLoading(false)
        }
        getData()
    }, [user, dateValue, authReady, notifications, ref] )

    return(
        <Layout>
            { 
            timeLogs.length ? (
                <>
            <section className="pt-10 container mx-auto" ref={svgRef}>
                <div className="flex flex-wrap gap-14">
                    <div className="dark:bg-slate-700 bg-gray-50 p-10 rounded-lg flex-1 min-w-[45rem]">
                        <div className="flex justify-between flex-1">
                            <InfoBox dateValue={dateValue} timeLogs={timeLogs} />
                            <DateSelector elector dateValue={ dateValue } onDateChange={ handleDateChange } />
                        </div>

                                                        
                        <ul className="flex w-full gap-6 mt-6 dark:bg-slate-700 rounded">
                            <li className="flex-1">
                                <input type="radio" id="stack" name="chart-type" value="stack" className="hidden peer" checked={chartType==='stack'} onChange={(e) => setChartType(e.target.value)}/>
                                <label htmlFor="stack" className="inline-flex h-full w-full p-5 text-gray-500 border rounded-lg cursor-pointer dark:border-gray-800 dark:peer-checked:text-purple-500 dark:peer-checked:border-purple-500 peer-checked:border-orange-500 peer-checked:text-orange-500 hover:text-gray-600 hover:bg-gray-100 dark:text-gray-300 dark:bg-gray-800 dark:hover:bg-gray-900">                           
                                    <div className="block">
                                        <div className="w-full font-semibold text-md">Stack</div>
                                        <div className="w-full text-sm">Total time tracked per day</div>
                                    </div>
                                </label>
                            </li>
                            <li className="flex-1">
                                <input type="radio" id="spread" name="chart-type" value="spread" className="hidden peer" checked={chartType==='spread'} onChange={(e) => setChartType(e.target.value)}/>
                                <label htmlFor="spread" className="inline-flex h-full w-full p-5 text-gray-500 border rounded-lg cursor-pointer dark:border-gray-800 dark:peer-checked:text-purple-500 dark:peer-checked:border-purple-500 peer-checked:border-orange-500 peer-checked:text-orange-500 hover:text-gray-600 hover:bg-gray-100 dark:text-gray-300 dark:bg-gray-800 dark:hover:bg-gray-900">
                                    <div className="block">
                                        <div className="w-full font-semibold text-md">Spread</div>
                                        <div className="w-full text-sm">Tasks spread over the day</div>
                                    </div>
                                </label>
                            </li>
                            <li className="flex-1">
                                <input type="radio" id="stream" name="chart-type" value="stream" className="hidden peer" checked={chartType==='stream'} onChange={(e) => setChartType(e.target.value)}/>
                                <label htmlFor="stream" className="inline-flex h-full w-full p-5 text-gray-500 border rounded-lg cursor-pointer dark:border-gray-800 dark:peer-checked:text-purple-500 dark:peer-checked:border-purple-500 peer-checked:border-orange-500 peer-checked:text-orange-500 hover:text-gray-600 hover:bg-gray-100 dark:text-gray-300 dark:bg-gray-800 dark:hover:bg-gray-900">
                                    <div className="block">
                                        <div className="w-full font-semibold text-md">Stream</div>
                                        <div className="w-full text-sm">Projects you've been working on</div>
                                    </div>
                                </label>
                            </li>
                        </ul>
                    </div>

                    <div className="flex-1 dark:bg-slate-700 bg-gray-50 p-10 rounded-lg min-w-[40rem]">
                        {   
                        loading ?
                            (<>
                                <h3 className="mb-5 text-lg">Popular items</h3>
                                <Loader height={200} /> 
                            </>) :
                            <PopularTasks tasks={timeLogs} handleHover={handleHover} />
                        }
                    </div>
                </div>
            </section>
            <section className="mx-auto container md:block hidden" ref={ref}>
  
                { ( chartType === 'stack' || chartType === 'spread' ) && <BarChart data={timeLogs} chartType={chartType} width={width} height={height} date={dateValue} hoverId={hoverId} margin={margin} /> }
                { streamChart }
                    
            </section>
            <section>
                
                <AnimatePresence
                    initial={false}
                    exitBeforeEnter={true}
                    onExitComplete={() => null}
                >
                    {modalOpen && <Modal modalOpen={modalOpen} handleClose={close} addTask={addLocalTask} tasks={timeLogs} />}
                </AnimatePresence>
                
                <section className="container py-5 mx-auto">
                    <div className="flex justify-between">
                        <div>
                            <h2 className="text-lg mb-2">Repeating time blocks</h2>
                            <p>Add in repeating blocks of time, to make it easier to track recurring tasks</p>
                        </div>
                        <div className="mt-8">
                            <motion.button
                                whileHover={{ scale: 1.05 }}
                                whileTap={{ scale: 0.9 }}
                                className="px-5 py-2 dark:text-white rounded-md dark:bg-slate-700 bg-gray-100 dark:hover:bg-slate-600"
                                onClick={ () => (modalOpen ? close() : open()) }
                            >
                                Add repeating time block
                            </motion.button>
                        </div>
                    </div>
                    <div className="flex mt-4 flex-wrap gap-5">
                        <RepeatingTime user={user?.id} handleRerender={handleRerender} repeatingTasks={repeatingTasks} addTask={addTask} removeLocalTask={removeLocalTask} addErrorNotification={addErrorNotification} />
                    </div>
                    
                </section>
                
                <Notifications notifications={notifications} removeNotification={removeNotifications} />
            </section>
            </>
            ) : <Loader /> }
        </Layout>
    )
}
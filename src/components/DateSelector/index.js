import React from "react";
import { formatDate } from '../../utils'

export default function DateSelector( { dateValue, onDateChange } ) {
    const today = formatDate( new Date() )
    const monthAgo = formatDate( new Date(Date.now() - 45 * 24 * 60 * 60 * 1000) )

    return(
        <div className="flex flex-col">
            <label className="mb-2" htmlFor="date">Select start date</label> 
            <input id="date" className="dark:bg-gray-800 dark:hover:bg-slate-900 rounded-lg px-4 py-2 cursor-pointer transition-all bg-white hover:bg-amber-50" onChange={ e => onDateChange(e.target.value) } list="days" type="date" value={dateValue} min={monthAgo} max={today} />
        </div>
    )
}
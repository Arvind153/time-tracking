import React, { useState, useEffect } from 'react'
import { logTime, getTask } from '../../api'
import { CloseButton } from "../Buttons/close-button";
import { motion } from "framer-motion"

const TimeCard = ({ task, user, removeLocalTask, addTask, addErrorNotification }) => {
    // const [ tasks, setTasks ] = useState([]);
    const dateValue = new Date()
    const year = dateValue.getFullYear()
    const month = ("0"+(dateValue.getMonth()+1)).slice(-2)
    const day = ("0" + dateValue.getDate()).slice(-2)
    const [ date, setDate ] = useState(`${year}-${month}-${day}`);
    const [ startTime, setStartTime ] = useState(task?.startTime);
    const [ name, setName ] = useState(false);
    const [ duration, setDuration ] = useState(task?.duration);
    
    // const [ tasks, setTasks ] = useState([]);

    useEffect( () => {
        getTask( task?.taskId ).then( log => {
            task['taskName'] = log?.task?.name
            setName(log?.task?.name)
        })
    }, [task, user])

    const updateStartTime = value => {
        setStartTime( value )
        updateTask()
    }

    const updateDuration = value => {
        setDuration( value )
        updateTask()
    }

    const updateTask = () => {
        const repeatingTasks = JSON.parse( localStorage.getItem("tw-repeating-tasks") ) || []
        const updatedTasks = repeatingTasks.filter( repeatingTask => !( 
            repeatingTask.taskId === task?.taskId && 
            repeatingTask?.startTime === task?.startTime && 
            repeatingTask?.duration === task?.duration 
            ) 
        )
        localStorage.setItem("tw-repeating-tasks", JSON.stringify( updatedTasks ));
    }

    const logTimeHandler = () => {
        const id = task?.taskId
        logTime( id, startTime, duration, date, user )
        .then( res => {
            if( res?.errors ) {
                addErrorNotification({error: res.errors[0]})
            } else {
                addTask({name, duration, startTime, date, id});
            }
        })
    }

    const deleteTimeLogHandler = () => {        
        const repeatingTasks = JSON.parse( localStorage.getItem("tw-repeating-tasks") ) || []

        const updatedTasks = repeatingTasks.filter( repeatingTask => {
            return !( repeatingTask.taskId === task?.taskId && repeatingTask?.startTime === task?.startTime && repeatingTask?.duration === task?.duration ) 
        })
        localStorage.setItem("tw-repeating-tasks", JSON.stringify( updatedTasks ));
        removeLocalTask(task)
    }
    
    return (
        <>
            {name && <h3 className="mb-5 text-xl">{name}</h3>}
            <div className="flex items-center mb-2"><span className="w-2/5 text-gray-400 dark:text-gray-400">Task ID:</span><span className="w-2/5">{task?.taskId}</span></div>
            <div className="flex items-center mb-2"><span className="w-2/5 text-gray-400 dark:text-gray-400">Start time:</span> <input className="w-3/5 px-2 py-1 dark:bg-slate-800 dark:hover:bg-gray-900 rounded" type="time" value={startTime} onChange={ e => updateStartTime(e.target.value) } /></div>
            <div className="flex items-center mb-2"><div className="w-2/5 text-gray-400 dark:text-gray-400"><span className="text-gray-400 dark:text-gray-400">Duration:</span><span className="block">minutes</span></div><input className="w-3/5 px-2 py-1 mr-1 dark:bg-slate-800 dark:hover:bg-gray-900 rounded" value={duration} type="number" onChange={ e => updateDuration(e.target.value) } /></div>
            <div className="flex items-center mb-2"><span className="w-2/5 text-gray-400 dark:text-gray-400">Date: </span><input className="w-3/5 px-2 py-1 dark:bg-slate-800 dark:hover:bg-gray-900 rounded" type="date" value={date} onChange={e => setDate(e.target.value)} /></div>
            <motion.button className="w-full px-4 py-2 mt-5 rounded-md dark:bg-slate-800 dark:hover:bg-gray-900 bg-white" onClick={logTimeHandler} whileHover={{ scale:1.05 }} whileTap={{ scale: 0.9 }}>Log</motion.button>
            <CloseButton close={deleteTimeLogHandler} dim={20} />
        </>
    );
  };

  
export default TimeCard;

import React from 'react'
import TimeCard from './TimeCard'
import { motion, AnimatePresence } from "framer-motion";

const RepeatingTime = ({ user, repeatingTasks, addTask, removeLocalTask, addErrorNotification }) => {    
    return (
        
            <AnimatePresence>
                    { repeatingTasks && repeatingTasks.map( ( task, i ) => 
                        <motion.div
                            layout 
                            key={task?.id}
                            className="relative flex flex-col justify-between max-w-md p-4 px-8 rounded-lg bg-gray-50 dark:bg-slate-700 basis-80"
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1, delay: 0.3*i }}
                            exit={{ opacity: 0 }}
                            data-key={ task?.id }
                        >
                            <TimeCard task={task} key={task?.taskId} user={user} removeLocalTask={removeLocalTask} addTask={addTask} addErrorNotification={addErrorNotification} />
                        </motion.div>
                    )}
            </AnimatePresence>
       
    );
  };

  
export default RepeatingTime;

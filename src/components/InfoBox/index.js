import React, { useContext } from "react";
import AuthContext from '../../context/authContext'
import { getBusinessDatesCount } from '../../utils'
// import { getUserDetails } from '../../api'
// import { getTime, getTask, getTimeTotals } from '../../api'

export default function InfoBox( { dateValue, timeLogs } ) {
    // const [ time, setTime ] = useState(false)
    const { user } = useContext(AuthContext)
    const date = dateValue
    const formatedDate = new Date(dateValue)
    const workDays = getBusinessDatesCount( formatedDate, new Date() )
    // console.log({timeLogs})
    const totalTime = timeLogs.reduce( ( total, num ) => total + num.hours, 0 )
    // console.log({user})
    // getUserDetails( user?.id ).then( res => console.log(res) )

    return(
        <section className="">
            {user && <h3 className="text-lg">Hi {user.firstName}</h3>}
            { timeLogs && (
                <div>
                    <p>Since {date}, there have been {workDays} work days or { workDays * 7 } hours.</p>
                    <p>You've logged { Number(totalTime / 7).toFixed(2) } days of work or {totalTime.toFixed(2)} hours.</p>
                    <p>So you've tracked <span className="font-bold">{ Number( ( totalTime / (workDays * 7) ) * 100).toFixed(2) }%</span> of time.</p>
                </div>
            ) }
        </section>
    )
}
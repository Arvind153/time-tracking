import { motion } from 'framer-motion';

export const Loader = ({ height = 450 }) => {
    const array = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    return (
        <div style={{height}} className="w-11/12 max-w-5xl relative py-5 mx-auto">
            <motion.div className="absolute flex justify-center h-20 gap-5 -translate-x-1/2 -translate-y-1/2 left-2/4 top-2/4">
                { array.map( ( a, i ) => (
                    <motion.span 
                        className="w-10 h-10 bg-orange-500 rounded dark:bg-purple-700" 
                        key={i} 
                        animate={{
                            height: [ "100px", "10px" ],
                            // y: [ "100px", "10px" ]
                        }}
                        transition={{
                            delay: 0.3*i,
                            flip: Infinity,
                            duration: 1.5,
                            ease: "easeInOut",
                        }}
                     />
                ) ) }
            </motion.div>
        </div>
    )
}
import { CogIcon } from '@heroicons/react/outline'
import useDarkMode from '../../hook/useDarkMode'
import { Link } from "react-router-dom";
import AuthContext from '../../context/authContext'
import React, { useContext } from 'react'
import { Sun } from '../../icons/Sun'
import { Moon } from '../../icons/Moon'

export default function Header() {
    const [ colorTheme, setTheme ] = useDarkMode()
    const { user } = useContext(AuthContext)
    // console.log( {user} )
    return (
        <nav className="h-16 bg-stone-100 transition dark:bg-slate-900">
            <div className="h-16 flex items-center justify-between max-w-5xl dark:text-white mx-auto">
                <Link to="/" className="text-lg dark:hover:text-purple-500 hover:text-orange-500 duration-300 font-mono transition-all">
                    <span className="dark:text-purple-500 text-orange-500 font-semibold">T</span>ime <span className="dark:text-purple-500 text-orange-500 font-semibold">T</span>racking
                </Link>
                <div className="flex gap-4">
                    <Link to="/settings">
                        {
                            user ?
                            <img className="h-10 w-10 rounded-full" src={user.avatarUrl} alt="me" /> :
                            <CogIcon className="h-10 w-10 cursor-pointer"  />
                        }
                    </Link>
                    { 
                    colorTheme === 'dark' ?
                    <button className="cursor-pointer" onClick={ () => setTheme(colorTheme) }><Moon /></button> :
                    <button className="cursor-pointer" onClick={ () => setTheme(colorTheme) }><Sun /></button>
                    }
                </div>
            </div>
        </nav>
    )
}

export const Tooltip = ({ opacity, text, y }) => {
	
    return (
		<text x={50} y={y} style={{ opacity: opacity}} className="label-y text-xl dark:fill-white" >
			{text}
		</text>
    );
};
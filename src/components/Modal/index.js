// import { motion } from "framer-motion";
import Backdrop from "./Backdrop";
import React, { useState, useEffect } from 'react'
import { CloseButton } from '../Buttons/close-button'
import { motion, AnimatePresence } from "framer-motion";

const dropIn = {
    hidden: {
      y: "-100vh",
      opacity: 0,
    },
    visible: {
      y: "0",
      opacity: 1,
      transition: {
        duration: 0.1,
        type: "spring",
        damping: 25,
        stiffness: 500,
      },
    },
    exit: {
      y: "100vh",
      opacity: 0,
    },
  };

const Modal = ({ handleClose, addTask, tasks }) => {
    const [ popularTask, setPopularTask ] = useState('');
    const [ startTime, setStartTime ] = useState('');
    const [ duration, setDuration ] = useState('');
    const [ valid, setValid ] = useState(false);
    const [ data, setData ] = useState([]);
    
    const handleStartTimeInput = event => setStartTime( event.target.value )
    const handleDurationInput = event => setDuration( event.target.value )
    const handleSelect = event => setPopularTask( event.target.value )

    useEffect( () => {
        if( popularTask && startTime && duration ) {
          setValid(true)
        } else {
          setValid(false)
        }

        const uncompleted = tasks?.filter( task => !task?.completed )
        const unique = [...new Map( uncompleted.map( item => ( [item['taskName'], item] ) )).values() ];
        const sortedTaskDetails = unique.sort((a, b) => a.taskName !== b.taskName ? a.taskName < b.taskName ? -1 : 1 : 0)
        setData( sortedTaskDetails )
    
    }, [popularTask, startTime, duration, tasks])

    const handleSubmit = event => {
        const payload = {
            taskId: popularTask,
            startTime,
            duration,
            id: new Date().getTime()
        }

        const repeatingTasks = JSON.parse( localStorage.getItem("tw-repeating-tasks") ) || []
        
        localStorage.setItem("tw-repeating-tasks", JSON.stringify( [...repeatingTasks, payload ] ));

        event.preventDefault()
        handleClose()
        addTask( payload )
    } 

    return (
      <Backdrop onClick={handleClose}>
          <motion.div
            onClick={(e) => e.stopPropagation()}  
            className="relative flex flex-col w-11/12 max-w-xl gap-3 p-5 py-10 mx-auto bg-gray-50 dark:bg-slate-900 rounded-xl"
            variants={dropIn}
            initial="hidden"
            animate="visible"
            exit="exit"
          >
            <CloseButton close={ () => handleClose() } dim={30} />
            <h2 className="text-xl">Add a repeating block</h2>
            <form onSubmit={handleSubmit} >
              <AnimatePresence>
                  <motion.label
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1 }} 
                    className="flex flex-col mb-2"
                  >Select an uncompleted task
                      <select className="p-2 mt-1 dark:bg-slate-700 bg-gray-100" type="text" onChange={handleSelect} value={popularTask}>
                        <option selected value=''>Select task</option>
                        { data && data.map( task => { 
                          if( !task?.taskId ) return '';
                          return( <option value={task?.taskId}>{task?.taskName} || {task?.project}</option>) }) }
                      </select>
                  </motion.label>
                
                
                <label className="flex flex-col mb-2">Start Time
                    <input className="p-2 mt-1 dark:bg-slate-700 bg-gray-100" type="time" onChange={handleStartTimeInput} value={startTime} />
                </label>

                <label className="flex flex-col mb-2">Duration in minutes
                    <input className="p-2 mt-1 dark:bg-slate-700 bg-gray-100" type="number" min="0" onChange={handleDurationInput} value={duration} />
                </label>

                {valid && <motion.button
                  layout
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  whileHover={{ scale: 1.05 }}
                  whileTap={{ scale: 0.9 }}
                  className="float-right px-4 py-2 mt-5 rounded-md dark:bg-slate-700 bg-gray-100" 
                  onClick={handleSubmit}
                >Add</motion.button>}
                </AnimatePresence>
              </form>
          </motion.div>
      </Backdrop>
    );
  };

  
export default Modal;

import AuthContext from '../../context/authContext'
import React, { useContext } from 'react'

export default function Footer() {
    const { user } = useContext(AuthContext)
    return (
        <footer className="h-16 p-5 text-white bg-slate-900">
            <div className="flex justify-between max-w-5xl mx-auto">
                <p>v1.0.3 ~ Built with <a className="transition-all hover:text-orange-500 dark:hover:text-purple-500" href="https://reactjs.org">React</a>, <a className="transition-all hover:text-orange-500 dark:hover:text-purple-500" href="https://tailwindcss.com">Tailwind</a>, <a className="transition-all hover:text-orange-500 dark:hover:text-purple-500" href="https://d3js.org">D3</a> & <a className="transition-all hover:text-orange-500 dark:hover:text-purple-500" href="https://www.framer.com/motion/">Framer motion</a></p>
                {user && <p>You've logged into Teamwork {user.loginCount} times</p>}
            </div>
        </footer>
    )
}
import { formatDate, CodeError } from '../utils'
var base64 = require('base-64');

function getHeader(key) {
  return  {
      headers: {
        Authorization: 'Basic ' + base64.encode(key + ":x"),
        'Content-Type': 'application/json'
      }
  }
}

export function validateKey(key) {
    if(
        key &&
        key.trim().length > 30 && 
        key.trim().length < 40) {
        return true
    }
    return false
}

export async function getTime(id, startDate) {
  try {
    const endDateObj = new Date();
    const endYear = endDateObj.getFullYear()
    const endMonth = ("0"+(endDateObj.getMonth()+1)).slice(-2)
    const endDay = ("0" + endDateObj.getDate()).slice(-2)
    const endDate = `${endYear}-${endMonth}-${endDay}`

    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )

    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v3/time.json?assignedToUserIds=${id}&startDate=${startDate}&endDate=${endDate}&pageSize=500`, getHeader(key))
    const resJson = await res.json()

    if( !resJson?.timelogs ) throw new CodeError( 'Bad request', 400 )
    let payload = []
    for( let i = 0; i < resJson?.timelogs.length; i++) {
      const log = resJson.timelogs[i]
      const date = new Date( log?.timeLogged )
      const hours = log?.minutes / 60
      const taskId = log?.taskId
      const id = log?.id
      const projectId = log?.projectId
      payload.push( { date, hours, taskId, id, projectId } )
    }
    
    if( !payload.length || formatDate( payload[0]?.date ) !== startDate ) {
        payload.unshift( { date: new Date(startDate), hours: 0 } )
    }
    if( formatDate( payload[payload?.length - 1].date ) !== formatDate( new Date() ) ) {
        payload.push( { date: new Date(), hours: 0 } )
    }

    return payload
  }
  catch(e) {
    console.error(e)
    return []
  }
}

export async function getUserDetails( id ) {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v3/people/${id}.json`, getHeader(key))
    const resJson = await res.json()
    if( resJson.STATUS !== 'OK' ) throw new CodeError( 'Bad request', 400 )
    return resJson.person
  }
  catch (e) {
    return e
  }
} 

export async function getTask( taskId ) {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    if( !taskId || taskId === undefined ) throw new CodeError( 'Task id not valid', 401 )
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v2/tasks/${taskId}.json`, getHeader(key))
    const resJson = await res.json()

    if( !resJson?.task ) throw new CodeError( resJson?.error, 400 )
   
    return resJson
  }
  catch (e) {
    return e
  }
}

export async function getProject( projectId ) {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    if( !projectId || projectId === undefined ) throw new CodeError( 'Task id not valid', 401 )
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v3/projects/${projectId}.json`, getHeader(key))
    const resJson = await res.json()

    if( !resJson?.project ) throw new CodeError( resJson?.error, 400 )
    return resJson
  }
  catch (e) {
    return e
  }
}

export async function logTime( taskId, startTime, duration, date, user ) {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    const options = getHeader(key)

    const data = {
      timelog: {
        "date": date,
        "description": "",
        "hasStartTime": true,
        "isBillable": true,
        "isUtc": false,
        "minutes": +duration,
        "userId": user,
        "time": startTime + ":00"
      }
    }
    options.method = 'POST'

    options.body = JSON.stringify(data)
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v3/tasks/${taskId}/time.json`, options)
    const resJson = await res.json()
    if( !resJson ) throw new CodeError( 'Bad request', 400 )

    return resJson
  }
  catch (e) {
    return e
  }
}

export async function getTimeTotals( id, date ) {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v3/time/total.json?assignedToUserIds=${id}&startDate=${date}`, getHeader(key))
    const resJson = await res.json()
    if( !resJson['time-totals'] ) throw new CodeError( 'Bad request', 400 )
    return resJson['time-totals']
  }
  catch (e) {
    return e
  }
}

export async function getUser() {
  try {
    const key = getKey()
    if( !validateKey(key) ) throw new CodeError( 'API key not valid', 401 )
    const res = await fetch(`https://seoworksshef.teamwork.com/projects/api/v2/me.json`, getHeader(key))
    const resJson = await res.json()
    if( resJson.STATUS !== 'OK' ) throw new CodeError( 'Bad request', 400 )
    return resJson.person
  }
  catch (e) {
    return e
  }
}

export function getKey() {
  const key = JSON.parse( localStorage.getItem("teamwork-api-key") )
  return key
}
export function formatDate( dateValue ) {
    const year = dateValue.getFullYear()
    const month = ("0"+(dateValue.getMonth()+1)).slice(-2)
    const day = ("0" + dateValue.getDate()).slice(-2)
    return `${year}-${month}-${day}`
}

export function getBusinessDatesCount(startDate, endDate) {
    let count = 0;
    const curDate = new Date(startDate.getTime());
    while (curDate <= endDate) {
        const dayOfWeek = curDate.getDay();
        if(dayOfWeek !== 0 && dayOfWeek !== 6) count++;
        curDate.setDate(curDate.getDate() + 1);
    }
    // alert(count);
    return count;
}

export class CodeError extends Error {
    constructor(message, code) {
     super(message);
     this.code = code;
    }
}
 
